import numpy as np
import re
import csv
import math
from nltk.tokenize import sent_tokenize, word_tokenize
from numpy.random import RandomState
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
import pandas as pd
from nltk.stem import WordNetLemmatizer
import sys

STOP_WORDS_SET = set()


class Document(object):
    '''
    Splits a text file into an ordered list of words.
    '''

    # List of punctuation characters to scrub. Omits, the single apostrophe,
    # which is handled separately so as to retain contractions.
    PUNCTUATION = ['(', ')', ':', ';', ',', '-', '!', '.', '?', '/', '"', '*']

    # Carriage return strings, on *nix and windows.
    CARRIAGE_RETURNS = ['\n', '\r\n']

    # Final sanity-check regex to run on words before they get
    # pushed onto the core words list.
    WORD_REGEX = "^[a-z']+$"
    #WORD_REGEX = ""

    def __init__(self):
        '''
        Set source file location, build contractions list, and initialize empty
        lists for lines and words.
        '''

        # self.file = open(self.filepath)
        self.lines = []
        self.words = []
        self.porter = PorterStemmer()
        self.lemmatizer = WordNetLemmatizer()


    def split(self, words, STOP_WORDS_SET):
        '''
        Split file into an ordered list of words. Scrub out punctuation;
        lowercase everything; preserve contractions; disallow strings that
        include non-letters.
        '''


        for word in words:
            clean_word = self._clean_word(word)
            if clean_word and (clean_word not in STOP_WORDS_SET) and (len(clean_word) > 1):  # omit stop words
                self.words.append(clean_word)

    def _clean_word(self, word):
        '''
        Parses a space-delimited string from the text and determines whether or
        not it is a valid word. Scrubs punctuation, retains contraction
        apostrophes. If cleaned word passes final regex, returns the word;
        otherwise, returns None.
        '''
        word = word.lower()
        for punc in Document.PUNCTUATION + Document.CARRIAGE_RETURNS:
            word = word.replace(punc, '').strip("'")
            word = self.stemWord(word)
        return word if re.match(Document.WORD_REGEX, word) else None

    def stemWord(self,word):
        return self.porter.stem(word)

    def lemmatizerWord(self,word):
        return self.lemmatizer.lemmatize(word)

class Corpus(object):
    """
    A collection of documents.
    """

    def __init__(self, documents_path, topic='1'):
        """
        Initialize empty document list.
        """
        self.documents = []
        self.vocabulary = []
        self.likelihoods = []
        self.documents_path = documents_path
        self.term_doc_matrix = None
        self.term_frequency_doc_matrix = None
        self.total_term_count = None
        self.idf = None
        self.tf_idf_matrix = None
        self.tf_idf_weight =None
        self.sum_tf_idf_weight = None
        self.sum_term_count = None
        self.term_exists_in_document = None
        self.topic = topic
        self.number_of_documents = 0
        self.vocabulary_size = 0



    def add_document(self, document):
        '''
        Add a document to the corpus.
        '''
        self.documents.append(document)


    def build_corpus(self):

        with open(self.documents_path) as csv_file:

            has_header = csv.Sniffer().has_header(csv_file.read(1024))
            csv_file.seek(0)
            # skip first row
            csv_reader = csv.reader(csv_file, delimiter=',')

            if has_header:
                next(csv_reader)  # Skip header row.

            # extracting each data row one by one
            for row in csv_reader:
                if(len(row) == 3 and (row[2] == self.topic or self.topic == '-1')): #skip row if it's less than 3
                    document = Document()  # instantiate document
                    document.split(row[1].split(), STOP_WORDS_SET)  # tokenize
                    self.add_document(document)  # push onto corpus documents list


        self.number_of_documents = len(self.documents)

    def build_vocabulary(self):

        discrete_set = set()
        for document in self.documents:
            for word in document.words:
                discrete_set.add(word)
        self.vocabulary = list(discrete_set)
        self.vocabulary_size = len(self.vocabulary)


    def build_term_doc_matrix(self):
        self.term_doc_matrix = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.float)
        self.term_frequency_doc_matrix = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.float)
        self.total_term_count = np.zeros([self.vocabulary_size], dtype=np.float)
        self.term_exists_in_document = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.int)

        for d_index, doc in enumerate(self.documents):
            term_count = np.zeros(self.vocabulary_size, dtype=np.int)
            total_term_occurance = np.zeros([self.vocabulary_size], dtype=np.int)
            term_frequency = np.zeros(self.vocabulary_size, dtype=np.float)

            #calculate terms per document
            for word in doc.words:
                if word in self.vocabulary:
                    w_index = self.vocabulary.index(word)
                    term_count[w_index] = term_count[w_index] + 1
                    total_term_occurance[w_index] = 1

            self.term_doc_matrix[d_index] = term_count
            self.term_exists_in_document[d_index]  = total_term_occurance


            #calculate term frequency per document
            for word in doc.words:
                if word in self.vocabulary:
                    w_index = self.vocabulary.index(word)
                    term_frequency[w_index] = term_count[w_index] / len(doc.words)

            self.term_frequency_doc_matrix[d_index] = term_frequency


        # print(self.term_doc_matrix)
        # print(self.term_frequency_doc_matrix)
        #print('Term exists in document')
        #print(self.term_exists_in_document)

        #sum of occurance of a term
        self.total_term_count = self.term_doc_matrix.sum(axis=0)
        #print('sum of occurance of a term:' + str(self.total_term_count))

        self.sum_term_count = np.sum(self.total_term_count)

        # w_index = self.vocabulary.index('a')
        # print('sum of occurance of a term a:' + str(self.total_term_count[w_index]))

        #calculate idf for all documents
        self.calculate_idf()

        #calculate tf-idf per term in each document
        self.calculate_tfidf()



    def calculate_idf(self):
        self.idf = np.zeros([self.vocabulary_size], dtype=np.float)

        term_in_document = self.term_exists_in_document.sum(axis=0)

        for word in self.vocabulary:
            if word in self.vocabulary:
                w_index = self.vocabulary.index(word)
                self.idf[w_index] = math.log( self.number_of_documents / term_in_document[w_index])

    def calculate_tfidf(self):
        self.tf_idf_matrix = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.float)

        for d_index, doc in enumerate(self.documents):
            tf_idf_document = np.zeros(self.vocabulary_size, dtype=np.float)

            #calculate terms per document
            for word in doc.words:
                w_index = self.vocabulary.index(word)
                tf_idf_document[w_index] = self.term_frequency_doc_matrix[d_index][w_index] * self.idf[w_index]


            self.term_doc_matrix[d_index] = tf_idf_document

        self.tf_idf_weight = self.term_doc_matrix.sum(axis=0)

        for word in self.vocabulary:
            if word in self.vocabulary:
                w_index = self.vocabulary.index(word)
                #print(word + '\t\t\t' + str(self.tf_idf_weight[w_index]))

        self.sum_tf_idf_weight = np.sum(self.tf_idf_weight)
        #print('sum_tf_idf_weight SPORTS:'+ str(self.sum_tf_idf_weight))

    def tf_idf_weight_per_term(self, word):
        try:
            w_index = self.vocabulary.index(word)
            return self.tf_idf_weight[w_index]
        except:
            return 0

    def term_count(self, word):
        try:
            w_index = self.vocabulary.index(word)
            return self.total_term_count[w_index]
        except:
            return 0


class TwitterClassifier(object):
    p_sports = 1
    p_non_sports = 1
    corpus = None
    sports_corpus = None
    non_sports_corpus = None


    def __init__(self, documents_path):
        self.corpus = Corpus(documents_path=documents_path, topic='-1')  # instantiate corpus
        self.corpus.build_corpus()
        #print("Number of Corpus documents:" + str(len(self.corpus.documents)))
        self.corpus.build_vocabulary()
        #print(self.corpus.vocabulary)
        #print("Vocabulary size:" + str(len(self.corpus.vocabulary)))

        self.sports_corpus = Corpus(documents_path=documents_path, topic='0')  # instantiate corpus
        self.sports_corpus.build_corpus()
        #print("Number of HAPPY documents:" + str(len(self.sports_corpus.documents)))
        self.sports_corpus.build_vocabulary()
        #print(self.sports_corpus.vocabulary)
        #print("Vocabulary size of Sports documents:" + str(len(self.sports_corpus.vocabulary)))
        self.sports_corpus.build_term_doc_matrix()

        self.non_sports_corpus = Corpus(documents_path=documents_path, topic='1')  # instantiate corpus
        self.non_sports_corpus.build_corpus()
        #print("Number of DEPRESSED documents:" + str(len(self.non_sports_corpus.documents)))
        self.non_sports_corpus.build_vocabulary()
        #print(self.non_sports_corpus.vocabulary)
        #print("Vocabulary size of Non-Sports documents:" + str(len(self.non_sports_corpus.vocabulary)))
        self.non_sports_corpus.build_term_doc_matrix()

        self.p_sports = math.log(self.sports_corpus.number_of_documents / self.corpus.number_of_documents)
        self.p_non_sports = math.log(self.non_sports_corpus.number_of_documents / self.corpus.number_of_documents)

        #print(self.p_sports)
        #print(self.p_non_sports)
        #print('tf-idf weight for all terms:' + str(self.sports_corpus.sum_tf_idf_weight))
        #print('Total vocab size in corpus:' + str(self.corpus.vocabulary_size))

    def Naive_Bayes_Classify(self, query_list):
        query = 'game'
        query_list = query.lower().split()

        probability_query_sports = 0
        # probability_query_sports = 1
        for term in query_list:
            # Laplace smoothing
            # Naive Bayes using tf-idf weight per term
            probability_query_sports += math.log((self.sports_corpus.tf_idf_weight_per_term(term) + 1) / (
                        self.sports_corpus.sum_tf_idf_weight + self.corpus.vocabulary_size))
            # probability_query_sports *= ((sports_corpus.tf_idf_weight_per_term(term) + 1) / (sports_corpus.sum_tf_idf_weight + corpus.vocabulary_size))

            # Naive Bayes using term count
            # probability_query_sports *= (sports_corpus.term_count(term) + 1) / (sports_corpus.sum_term_count + corpus.vocabulary_size)

        probability_query_sports += self.p_sports
        # probability_query_sports *= p_sports
        #print('Probability Sports:')
        #print(probability_query_sports)

        probability_query_non_sports = 0
        # probability_query_non_sports = 1
        for term in query_list:
            # Laplace smoothing
            # Naive Bayes using tf-idf weight per term
            probability_query_non_sports += math.log((self.non_sports_corpus.tf_idf_weight_per_term(term) + 1) / (
                    self.non_sports_corpus.sum_tf_idf_weight + self.corpus.vocabulary_size))
            # probability_query_non_sports *=  ((non_sports_corpus.tf_idf_weight_per_term(term) + 1)/(non_sports_corpus.sum_tf_idf_weight + corpus.vocabulary_size))

            # Naive Bayes using term count
            # probability_query_non_sports *= (non_sports_corpus.term_count(term) + 1) / (non_sports_corpus.sum_term_count + corpus.vocabulary_size)

        probability_query_non_sports += self.p_non_sports
        # probability_query_non_sports *= p_non_sports
        # print('Probability Non-Sports:')
        # print(probability_query_non_sports)

        if (probability_query_sports >= probability_query_non_sports):
            #print('Classification:' + 'SPORTS')
            return 0
        else:
            #print('Classification:' + 'NOT SPORTS')
            return 1

    def predict(self, testData, csv_file_name):
        result = []
        for i, r in testData.iterrows():
            # print('Message:')
            # print(r['message'])
            # print('label:')
            # print(r['label'])
            processed_message = process_message(r['message'])
            result.append(int(self.Naive_Bayes_Classify(processed_message)))
            # testData['prediction'] = pd.Series(int(self.Naive_Bayes_Classify(processed_message)), index=testData.index)
            # testData = testData.assign(e=pd.Series(result[i]))

        testData['prediction'] = result
        testData.to_csv(csv_file_name + '_results.txt')
        return testData

    def metrics(self,testData):

        # for index, row in testData.iterrows():
        #     #print(row['label'], row['prediction'])
        #     label = int(row['label'])
        #     prediction = int(row['prediction'])
        #     print('Metric:')
        #     print(int(label == 1 and prediction == 1))


        true_pos, true_neg, false_pos, false_neg = 0, 0, 0, 0
        for index, row in testData.iterrows():
            label = int(row['label'])
            prediction = int(row['prediction'])

            true_pos += int(label == 0 and prediction == 0)
            true_neg += int(label == 1 and prediction == 1)
            false_pos += int(label == 1 and prediction == 0)
            false_neg += int(label == 0 and prediction == 1)

            # true_pos += int(label == 1 and prediction == 1)
            # true_neg += int(label == 0 and prediction == 0)
            # false_pos += int(label == 0 and prediction == 1)
            # false_neg += int(label == 1 and prediction == 0)

        precision = true_pos / (true_pos + false_pos)
        recall = true_pos / (true_pos + false_neg)
        Fscore = 2 * precision * recall / (precision + recall)
        accuracy = (true_pos + true_neg) / (true_pos + true_neg + false_pos + false_neg)

        print("Precision: ", precision)
        print("Recall: ", recall)
        print("F-score: ", Fscore)
        print("Accuracy: ", accuracy)

def process_message(message, lower_case = True, stem = True, stop_words = True, gram = 1):
    if lower_case:
        message = message.lower()
    words = word_tokenize(message)
    words = [w for w in words if len(w) > 1]
    if gram > 1:
        w = []
        for i in range(len(words) - gram + 1):
            w += [' '.join(words[i:i + gram])]
        return w
    if stop_words:
        sw = stopwords.words('english')
        words = [word for word in words if word not in sw]
    if stem:
        stemmer = PorterStemmer()
        words = [stemmer.stem(word) for word in words]
    return words



def main():
    if len(sys.argv) != 2:
        print("Usage: {} <name of CSV file>".format(sys.argv[0]))
        sys.exit(1)

    cfg = sys.argv[1]

    stopwordsfile = open("./stopwords.txt", "r")
    for word in stopwordsfile:  # a stop word in each line
        word = word.replace("\n", '')
        word = word.replace("\r\n", '')
        STOP_WORDS_SET.add(word)


    csv_file_name = cfg
    df = pd.read_csv(csv_file_name + '.csv')
    rng = RandomState()

    trainData = df.sample(frac=0.98, random_state=rng)
    testData = df.loc[~df.index.isin(trainData.index)]

    trainData.to_csv(csv_file_name + '_train.csv', index=False)
    testData.to_csv(csv_file_name + '_test.csv', index=False)

    documents_path = csv_file_name + '_train.csv'

    tweetClassifier = TwitterClassifier(csv_file_name + '_train.csv')

    results = tweetClassifier.predict(testData, csv_file_name)
    tweetClassifier.metrics(results)

    print('Output is in <name of CSV file>_results.csv, program appends prediction at end of each row')




if __name__ == '__main__':
    main()
