import numpy as np
import re
import csv
import math
import sys

#Documents in Corpus
class Document(object):
    '''
    Splits a text file into an ordered list of words.
    '''

    # List of punctuation characters to scrub. Omits, the single apostrophe,
    # which is handled separately so as to retain contractions.
    PUNCTUATION = ['(', ')', ':', ';', ',', '-', '!', '.', '?', '/', '"', '*']

    # Carriage return strings, on *nix and windows.
    CARRIAGE_RETURNS = ['\n', '\r\n']

    # Final sanity-check regex to run on words before they get
    # pushed onto the core words list.
    WORD_REGEX = "^[a-z']+$"
    #WORD_REGEX = ""

    def __init__(self):
        '''
        Set source file location, build contractions list, and initialize empty
        lists for lines and words.
        '''

        # self.file = open(self.filepath)
        self.lines = []
        self.words = []



    def split(self, words):
        '''
        Split file into an ordered list of words. Scrub out punctuation;
        lowercase everything; preserve contractions; disallow strings that
        include non-letters.
        '''


        for word in words:
            clean_word = self._clean_word(word)
            if clean_word:  # omit stop words:
                self.words.append(clean_word)

    def _clean_word(self, word):
        '''
        Parses a space-delimited string from the text and determines whether or
        not it is a valid word. Scrubs punctuation, retains contraction
        apostrophes. If cleaned word passes final regex, returns the word;
        otherwise, returns None.
        '''
        word = word.lower()
        for punc in Document.PUNCTUATION + Document.CARRIAGE_RETURNS:
            word = word.replace(punc, '').strip("'")
        return word if re.match(Document.WORD_REGEX, word) else None


#Corpus
class Corpus(object):
    """
    A collection of documents.
    """

    def __init__(self, documents_path, topic='1'):
        """
        Initialize empty document list.
        """
        self.documents = []
        self.vocabulary = []
        self.likelihoods = []
        self.documents_path = documents_path
        self.term_doc_matrix = None
        self.term_frequency_doc_matrix = None
        self.total_term_count = None
        self.idf = None
        self.tf_idf_matrix = None
        self.tf_idf_weight =None
        self.sum_tf_idf_weight = None
        self.sum_term_count = None
        self.term_exists_in_document = None
        self.topic = topic
        self.number_of_documents = 0
        self.vocabulary_size = 0



    def add_document(self, document):
        '''
        Add a document to the corpus.
        '''
        self.documents.append(document)


    def build_corpus(self):

        with open(self.documents_path) as csv_file:

            has_header = csv.Sniffer().has_header(csv_file.read(1024))
            csv_file.seek(0)
            # skip first row
            csv_reader = csv.reader(csv_file, delimiter=',')

            if has_header:
                next(csv_reader)  # Skip header row.

            # extracting each data row one by one
            for row in csv_reader:
                if(len(row) == 3 and (row[2] == self.topic or self.topic == '-1')): #skip row if it's less than 3
                    document = Document()  # instantiate document
                    document.split(row[1].split())  # tokenize
                    self.add_document(document)  # push onto corpus documents list


        self.number_of_documents = len(self.documents)

    #Unique terms in corpus
    def build_vocabulary(self):

        discrete_set = set()
        for document in self.documents:
            for word in document.words:
                discrete_set.add(word)
        self.vocabulary = list(discrete_set)
        self.vocabulary_size = len(self.vocabulary)

    #Build term document matrix
    #term_doc_matrix: Matrix of document (rows) & term as column, value is total terms per document
    #term_frequency_doc_matrix: Matrix of document (rows) & term as column, value is term frequency.
    #term_exists_in_document: Matrix of document (rows) & term as column, This matrix sets value to 1 in matrix if term exists in document.
    #term_exists_in_document: This is used in calculating IDF to find total documents containing respective term.
    def build_term_doc_matrix(self):
        self.term_doc_matrix = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.float)
        self.term_frequency_doc_matrix = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.float)
        self.total_term_count = np.zeros([self.vocabulary_size], dtype=np.float)
        self.term_exists_in_document = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.int)

        for d_index, doc in enumerate(self.documents):
            term_count = np.zeros(self.vocabulary_size, dtype=np.int)
            total_term_occurance = np.zeros([self.vocabulary_size], dtype=np.int)
            term_frequency = np.zeros(self.vocabulary_size, dtype=np.float)

            #calculate terms per document
            for word in doc.words:
                if word in self.vocabulary:
                    w_index = self.vocabulary.index(word)
                    term_count[w_index] = term_count[w_index] + 1
                    total_term_occurance[w_index] = 1

            self.term_doc_matrix[d_index] = term_count
            self.term_exists_in_document[d_index]  = total_term_occurance


            #calculate term frequency per document
            for word in doc.words:
                if word in self.vocabulary:
                    w_index = self.vocabulary.index(word)
                    term_frequency[w_index] = term_count[w_index] / len(doc.words)

            self.term_frequency_doc_matrix[d_index] = term_frequency

        print('Term Document Matrix:..')
        print(self.term_doc_matrix)
        print('Term Frequency Matrix:..')
        print(self.term_frequency_doc_matrix)
        print('Term exists in document')
        print(self.term_exists_in_document)

        #sum of occurance of a term
        self.total_term_count = self.term_doc_matrix.sum(axis=0)
        #print('sum of occurance of a term:' + str(self.total_term_count))

        self.sum_term_count = np.sum(self.total_term_count)

        # w_index = self.vocabulary.index('a')
        # print('sum of occurance of a term a:' + str(self.total_term_count[w_index]))

        #calculate idf for all documents
        self.calculate_idf()

        #calculate tf-idf per term in each document
        self.calculate_tfidf()


    #Calculates IDF per term in Corpus.
    def calculate_idf(self):
        self.idf = np.zeros([self.vocabulary_size], dtype=np.float)

        term_in_document = self.term_exists_in_document.sum(axis=0)

        for word in self.vocabulary:
            if word in self.vocabulary:
                w_index = self.vocabulary.index(word)
                self.idf[w_index] = math.log( self.number_of_documents / term_in_document[w_index])

    #Calculate tf-idf per term
    def calculate_tfidf(self):
        self.tf_idf_matrix = np.zeros([self.number_of_documents, self.vocabulary_size], dtype=np.float)

        for d_index, doc in enumerate(self.documents):
            tf_idf_document = np.zeros(self.vocabulary_size, dtype=np.float)

            #calculate terms per document
            for word in doc.words:
                w_index = self.vocabulary.index(word)
                tf_idf_document[w_index] = self.term_frequency_doc_matrix[d_index][w_index] * self.idf[w_index]


            self.term_doc_matrix[d_index] = tf_idf_document

        self.tf_idf_weight = self.term_doc_matrix.sum(axis=0)

        for word in self.vocabulary:
            if word in self.vocabulary:
                w_index = self.vocabulary.index(word)
                print(word + '\t\t\t' + str(self.tf_idf_weight[w_index]))

        self.sum_tf_idf_weight = np.sum(self.tf_idf_weight)
        print('sum_tf_idf_weight SPORTS:'+ str(self.sum_tf_idf_weight))

    def tf_idf_weight_per_term(self, word):
        try:
            w_index = self.vocabulary.index(word)
            return self.tf_idf_weight[w_index]
        except:
            return 0

    def term_count(self, word):
        try:
            w_index = self.vocabulary.index(word)
            return self.total_term_count[w_index]
        except:
            return 0


#Holds corpus for two topics(happy, depressed) and entire corpus
class TweetClassifier(object):
    p_sports = 1
    p_non_sports = 1
    corpus = None
    sports_corpus = None
    non_sports_corpus = None

    def __init__(self, documents_path):
        self.corpus = Corpus(documents_path=documents_path, topic='-1')  # instantiate corpus
        self.corpus.build_corpus()
        print("Number of Corpus documents:" + str(len(self.corpus.documents)))
        self.corpus.build_vocabulary()
        print(self.corpus.vocabulary)
        print("Vocabulary size:" + str(len(self.corpus.vocabulary)))

        self.sports_corpus = Corpus(documents_path=documents_path, topic='1')  # instantiate corpus
        self.sports_corpus.build_corpus()
        print("Number of Happy documents:" + str(len(self.sports_corpus.documents)))
        self.sports_corpus.build_vocabulary()
        print(self.sports_corpus.vocabulary)
        print("Vocabulary size of Non-depressed documents:" + str(len(self.sports_corpus.vocabulary)))
        self.sports_corpus.build_term_doc_matrix()

        self.non_sports_corpus = Corpus(documents_path=documents_path, topic='0')  # instantiate corpus
        self.non_sports_corpus.build_corpus()
        print("Number of depressed documents:" + str(len(self.non_sports_corpus.documents)))
        self.non_sports_corpus.build_vocabulary()
        print(self.non_sports_corpus.vocabulary)
        print("Vocabulary size of depressed documents:" + str(len(self.non_sports_corpus.vocabulary)))
        self.non_sports_corpus.build_term_doc_matrix()


        print('tf-idf weight for all terms:' + str(self.sports_corpus.sum_tf_idf_weight))
        print('Total vocab size in corpus:' + str(self.corpus.vocabulary_size))

    #classifies text into happy or depressed sentiment
    def Multinomial_Naive_Bayes_classifier(self, text):
        query_list = text.lower().split()

        self.p_sports = self.sports_corpus.number_of_documents / self.corpus.number_of_documents
        self.p_non_sports = self.non_sports_corpus.number_of_documents / self.corpus.number_of_documents

        print(self.p_sports)
        print(self.p_non_sports)


        probability_query_sports = 1
        for term in query_list:
            # Apply Laplace smoothing

            # Naive Bayes using tf-idf weight per term
            probability_query_sports *= ((self.sports_corpus.tf_idf_weight_per_term(term) + 1) / (
                    self.sports_corpus.sum_tf_idf_weight + self.corpus.vocabulary_size))

            # Naive Bayes using term count
            # probability_query_sports *= (sports_corpus.term_count(term) + 1) / (sports_corpus.sum_term_count + corpus.vocabulary_size)


        probability_query_sports *= self.p_sports
        print('Probability Depressed sentiment:')
        print(probability_query_sports)


        probability_query_non_sports = 1
        for term in query_list:
            # Apply Laplace smoothing

            # Naive Bayes using tf-idf weight per term
            probability_query_non_sports *= ((self.non_sports_corpus.tf_idf_weight_per_term(term) + 1) / (
                    self.non_sports_corpus.sum_tf_idf_weight + self.corpus.vocabulary_size))

            # Naive Bayes using term count
            # probability_query_non_sports *= (non_sports_corpus.term_count(term) + 1) / (non_sports_corpus.sum_term_count + corpus.vocabulary_size)


        probability_query_non_sports *= self.p_non_sports
        print('Probability Happy sentiment:')
        print(probability_query_non_sports)

        if (probability_query_sports >= probability_query_non_sports):
            return 'Happy'
        else:
            return 'Depressed'



if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Usage: {} sentiment_sample.csv \"<text to classify>\" ".format(sys.argv[0]))
        sys.exit(1)

    cfg = sys.argv[1]
    text_to_classify = sys.argv[2]

    classifyTweet = TweetClassifier(documents_path=cfg)
    result = classifyTweet.Multinomial_Naive_Bayes_classifier(text_to_classify)
    print('Sentiment of text \"' + text_to_classify + '\" is:' + result)