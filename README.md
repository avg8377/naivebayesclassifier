# Depression Detection in Tweets using Multinomial Naïve Bayes algorithm

## Overview

Team member: [Sunil Kulkarni](https://www.linkedin.com/in/sunil-kulkarni-624657/) : NetID: sunilk2

This is a final project for the course [CS410](https://www.coursera.org/learn/cs-410/home/welcome). Mental illness such as depression can be life threatening, with suicide as a possible outcome. 
In this project, Multinomial Naivee Bayees is used using tf-idf weights to determine whether social platform users are depressive based on their Twitter posts. 
The accuracy of the model is evaluated using test data using human and system classification prediction.
It is discovered that the model has a ~94% accuracy. Refer [Medium](https://medium.com/@sunilskulkarni/depression-detection-in-tweets-using-multinomial-na%C3%AFve-bayes-algorithm-89ce6ef491b) post for detailed hands-on implementation details. 
Basically, this tool potentially applies all the knowledge and skills learned in the course to solve a real-world problem. Tool interacts with user and classifies entered tweets as depressed or non-depressed. Also generates output file that has prediction of human and system test data.

## Requirements

- Python 3.6

## Install & How to run

**Git:**
```bash
git clone https://lab.textdata.org/sunilk2/Course_Project.git
```

**Run:**
```bash
cd Course_Project
python3 -m venv ./course_prj_venv
source course_prj_venv/bin/activate
pip3 install -r requirements.txt
python3 twitter_classifier_naive_bayes.py
```

Program runs in interactive mode where you can enter tweets and system will classify them as depressed or non-depressed.
 ```bash

(course_prj_venv) [sunilk2@linux-a3 Course_Project]$ python3 twitter_classifier_naive_bayes.py
[nltk_data] Downloading package punkt to /home/sunilk2/nltk_data...
[nltk_data]   Package punkt is already up-to-date!
[nltk_data] Downloading package stopwords to
[nltk_data]     /home/sunilk2/nltk_data...
[nltk_data]   Unzipping corpora/stopwords.zip.
----------------------Train classifer with training data set------------------------------
Reading Kaggle dataset twitter_sentiment data in file, sentiment_tweets3
Format of the CSV file >> id (document ID),message (tweet),label (sentiment)
Split data into 98% training data and 2% test data..
Train the classifier using training data set...
Converting a collection of ENTIRE Corpus to a matrix of TF-IDF features using TfidfVectorizer with ngram_range(1,2)..
Converting a NON-DEPRESSED Corpus to a matrix of TF-IDF features using TfidfVectorizer with ngram_range(1,2)..
Converting a DEPRESSED Corpus to a matrix of TF-IDF features using TfidfVectorizer with ngram_range(1,2)..
----------------------Apply classifier on test data------------------------------
Apply Multinomial Naïve Bayes algorithm classifier to generate sentiment i.e. reading tweet (message) from test data, predict if it is non-depressed(0) or depressed(1)
Applied classification on test-data, results are in output_testdata_prediction.txt file.. 
Format of the output_testdata_prediction CSV file >> id (document ID),message (tweet),label (given sentiment), prediction
----------------------------------------------------------
Calculating precision, re-call, accurancy, F-score on test-data prediction. This will compare human prediction and system prediction
Precision:  0.9567901234567902
Recall:  0.9935897435897436
F-score:  0.9748427672955975
Accuracy:  0.9611650485436893
----------------------------------------------------------

--- interact with tool, enter tweet and system will predict sentiment--

Enter a tweet? (quit to exit):I am depressed, need help
Sentiment:DEPRESSED tweet
Enter a tweet? (quit to exit):I am so happy today
Sentiment:NON-DEPRESSED tweet
Enter a tweet? (quit to exit):quit
```

**Output:**

Program reads `sentiment_tweets3.csv` & splits it into `training` &  `test` CSV. Classifier runs prediction on `test` CSV and generates `output_testdata_prediction.csv` which contains both human and system prediction. 
Precision, Recall, F-score and accuracy is calculated from `output_testdata_prediction.csv` that has human and system prediction. 

```sh
(course_prj_venv) [sunilk2@linux-a3 Course_Project]$ pwd
/home/sunilk2/Course_Project
(course_prj_venv) [sunilk2@linux-a3 Course_Project]$ vi output_testdata_prediction.csv 
```


## Video Demo
- Link to the video (TBD)

## License
MIT

## Implementation
Refer [Medium](https://medium.com/@sunilskulkarni/depression-detection-in-tweets-using-multinomial-na%C3%AFve-bayes-algorithm-89ce6ef491b) post for detailed hands-on implementation details with core concepts, algorithm and how classification is done on paper.
This tool/program has implemented some of the recommended further improvements. Following section will focus more on implementation than core concepts since it's covered well in Medium post.

#### Retrieving Test Data
There are two kinds of tweets that are needed for this project: random tweets that do not indicate depression and tweets that show the user may have depression. The random tweets dataset can be found from the Kaggle dataset [twitter_sentiment](https://www.kaggle.com/ywang311/twitter-sentiment/data). It is harder to get tweets that indicate depression as there is no public dataset of depressive tweets, so in this project tweets indicating depression are retrieved using the Twitter scraping tool [TWINT](https://github.com/haccer/twint) using the keyword `depression` by scraping all tweets in an one day span. The scrapped tweets may contain tweets that do not indicate the user having depression, such as tweets linking to articles about depression. As a result, the scrapped tweets need to be manually checked for better testing results. A csv file of scrapped tweets is provided, however the following code can be used to obtain depressive tweets for this project, keep in mind that the date in the code should be changed and the generated .csv file should be manually checked and moved to the project directory:
```sh
python3 twint.py -s depression --since 2018-05-15 -o depressive_tweets_processed.csv --csv
``` 
```sh
twint -s "#depressed" --since 2018-05-15 -o depressive_tweets_processed.csv --csv
``` 

All the above data is bundled into sentiment_tweets3.csv. You can improve accuracy by add additional samples of depressed and non-depressed tweets in the CSV file.
```sh
#sentiment_tweets3.csv is downloaded from https://www.kaggle.com/ywang311/twitter-sentiment/data and read.
df = pd.read_csv(csv_file_name + '.csv')
```


#### Split data into training and test data
Split data set into 98% trining data set and remaining 2% for test data set. 
Algorithm uses 98% of data to train and 2% of data is used to test prediction of the algorithm i.e. test data has human prediction of tweet, system evaluates and predicts. This way, we can compare between human and system to calculate metrics.

```sh
trainData = df.sample(frac=0.98, random_state=rng)git 
testData = df.loc[~df.index.isin(trainData.index)]
```

 
#### Corpus class
Reads Corpus and calculates the following per Corpus.
- Calculate total documents in Corpus
- Length of features (Unique vocabulary)
- tf-idf weights per term in Corpus
- total tf-idf weight for all terms.

Class converts a collection of raw documents to a matrix of TF-IDF features using [TfidfVectorizer](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html)
Refer [Medium](https://medium.com/@rnbrown/more-nlp-with-sklearns-countvectorizer-add577a0b8c8) for tuning TfidfVectorizer


```sh
vectorizer = TfidfVectorizer(stop_words='english', analyzer='word', token_pattern=u'(?ui)\\b\\w*[a-z]+\\w*\\b',
                             ngram_range=(1, 2))
```

This removes stop words uses ngram_range (2-grams). 

```sh
train_data_features = vectorizer.fit_transform(self.doc_generator(csv_file, topic, textcol=1, skipheader=True))
```
Apply [Stemming & lemmatization](https://towardsdatascience.com/stemming-lemmatization-what-ba782b7c0bd8) 

```sh
    def doc_generator(self,filepath, topic, textcol=0, skipheader=True):

        porter = PorterStemmer()
        lemmatizer = WordNetLemmatizer()
        with open(filepath) as f:
            reader = csv.reader(f)
            if skipheader:
                next(reader, None)
            if (topic == '-1'):
                for row in reader:
                   stem = self.stemSentence(porter, row[textcol])
                   yield stem
            else:
                for row in reader:
                    if (topic == row[2]):
                        stem = self.stemSentence(porter, row[textcol])
                        yield stem
```

In the program, I didn't call lemmatization but it can be called on top of stemming.

```sh
    def stemSentence(self,porter, sentence):
        token_words = word_tokenize(sentence)

        stem_sentence = []
        for word in token_words:
            stem_sentence.append(porter.stem(word))
            stem_sentence.append(" ")
        return "".join(stem_sentence)

    def lemmatizeSentence(self,lemmatizer, sentence):
        token_words = word_tokenize(sentence)

        stem_sentence = []
        for word in token_words:
            stem_sentence.append(lemmatizer.lemmatize(word))
            stem_sentence.append(" ")
        return "".join(stem_sentence)
```

#### Train the classifier using training data set.
`TweetClassifier` calls `Corpus` class for entire Corpus and also for depressed and non-depressed Corpus & extracts the following.
- Entire Corpus : Calculate total documents, length of features (Unique vocabulary)
- Non-depressed Corpus : Calculate total documents, tf-idf weights per term in Corpus & total tf-idf weight for all terms.
- Depressed Corpus : Calculate total documents, tf-idf weights per term in Corpus & total tf-idf weight for all terms.
- Calculate probability of non-depressed documents

```sh
        #Build entire corpus that includes depressed and non-depressed topics.
        self.Corpus = Corpus(csv_file, '-1')
        #vocabulary lengh (feature names length) in corpus.
        self.feature_names_size = self.Corpus.vocab_len
        #total docs in Corups
        self.total_docs = self.Corpus.rows_topic

        print(
            'Converting a NON-DEPRESSED Corpus to a matrix of TF-IDF features using TfidfVectorizer with ngram_range(1,2)..')
        #non-depressed corpus
        self.Corpus_Non_Depressed = Corpus(csv_file, '0')
        #total non-depressed documents
        self.Non_Depressed_docs = self.Corpus_Non_Depressed.rows_topic
        # sum of all tf-idf term weights for non-depressed documents
        self.non_depressed_sum_tf_idf_weights_all_terms = self.Corpus_Non_Depressed.sum_tf_idf_weights_all_terms

        print(
            'Converting a DEPRESSED Corpus to a matrix of TF-IDF features using TfidfVectorizer with ngram_range(1,2)..')
        # depressed corpus
        self.Corpus_Depressed = Corpus(csv_file, '1')
        self.Depressed_docs = self.Corpus_Depressed.rows_topic
        self.depressed_sum_tf_idf_weights_all_terms = self.Corpus_Depressed.sum_tf_idf_weights_all_terms
```

It also calculates probability of non-depressed documents & depressed documents which is used in Multinomial Naïve Bayes algorithm
```sh
        #probability of non-depressed documents
        self.p_non_depressed_topic = log(self.Non_Depressed_docs / self.total_docs)
        # probability of depressed documents
        self.p_depressed_topic = log(self.Depressed_docs / self.total_docs)
```

#### Naive_Bayes_Classify using logarithm & Laplace smoothing
This function performs classification of tweet into depressed or non-depressed sentiment. It first calculates `probability_non_depressed`
using Naive Bayes formula. Here tf-idf weight is used instead of term count which is improvement over using term count. Using tf-idf weight gives better accuracy.
In this case, tweet is read and probability on each term is calculated using it's tf-idf weight per term. Please note, `log` is used for increased precision while still maintaining the ordering. 

```sh
        #Calculate probability of non-depressed sentiment for given tweet
        probability_non_depressed = 0
        for term in tweet:
            # Apply Laplace smoothing
            tf_idf_per_term = self.Corpus_Non_Depressed.get_term_tf_idf(term)
            #Use log to preserve precision instead of multiplication
            probability_non_depressed += log((tf_idf_per_term + 1) / (self.non_depressed_sum_tf_idf_weights_all_terms + self.feature_names_size))
        probability_non_depressed += self.p_non_depressed_topic

        # Calculate probability of depressed sentiment for given tweet
        probability_depressed = 0
        for term in tweet:
            #Laplace smoothing
            tf_idf_per_term = self.Corpus_Depressed.get_term_tf_idf(term)
            probability_depressed += log((tf_idf_per_term + 1) / (self.depressed_sum_tf_idf_weights_all_terms + self.feature_names_size))
        probability_depressed += self.p_depressed_topic

        if (probability_non_depressed >= probability_depressed):
            return 0
        else:
            return 1
```

### Predict
This function will run prediction on test data CSV, reads tweet from CSV, one row at a time, calls classifier to classify.
```sh
#Run classification on test-data and let system predict sentiment and output it to a file.
    def predict(self,testData):
        result = []
        for i, r in testData.iterrows():
            processed_message = process_message(r['message'])
            result.append(int(self.Naive_Bayes_Classify(processed_message)))

        pd.options.mode.chained_assignment = None
        testData['prediction'] = result
        testData.to_csv('output_testdata_prediction.csv')
        print('Applied classification on test-data, results are in output_testdata_prediction.txt file.. ')
        print('Format of the output_testdata_prediction CSV file >> id (document ID),message (tweet),label (given sentiment), prediction')
        return testData
```

### Metrics
`Predict` function retuns `data frame` that has new column `prediction`.
Metics function compares human prediction (`label` column) and system prediction `prediction` column to find metrics.
```sh
    #refer, https://towardsdatascience.com/accuracy-precision-recall-or-f1-331fb37c5cb9
    #Run the metrics on system prediction on test-data and calculate metrics on human vs system prediction.
    def metrics(self,testData):
        print('Calculating precision, re-call, accurancy, F-score on test-data prediction. This will compare human prediction and system prediction')
        true_pos, true_neg, false_pos, false_neg = 0, 0, 0, 0
        for index, row in testData.iterrows():
            label = int(row['label'])
            prediction = int(row['prediction'])

            true_pos += int(label == 0 and prediction == 0)
            true_neg += int(label == 1 and prediction == 1)
            false_pos += int(label == 1 and prediction == 0)
            false_neg += int(label == 0 and prediction == 1)

            # true_pos += int(label == 1 and prediction == 1)
            # true_neg += int(label == 0 and prediction == 0)
            # false_pos += int(label == 0 and prediction == 1)
            # false_neg += int(label == 1 and prediction == 0)

        precision = true_pos / (true_pos + false_pos)
        recall = true_pos / (true_pos + false_neg)
        Fscore = 2 * precision * recall / (precision + recall)
        accuracy = (true_pos + true_neg) / (true_pos + true_neg + false_pos + false_neg)

        print("Precision: ", precision)
        print("Recall: ", recall)
        print("F-score: ", Fscore)
        print("Accuracy: ", accuracy)
```

### Interact with tool
You can interact with the tool by entering tweets and sytem will predict sentiment. Please note, accuracy isn't 100% so there will be false positives. Also refer further improvements section to scrape additional tweets using `TWINT` to improve metrics.
```sh
        print('\n\n\n*********Let\'s interact with tool, enter tweet and system will predict sentiment********\n\n\n')
        while True:
            tweet = input("Enter a tweet? (quit to exit):")
            if tweet == "quit":
                break
            else:
                processed_message = process_message(tweet)
                if (int(tweetClassifier.Naive_Bayes_Classify(processed_message))):
                    print('Sentiment:' + 'DEPRESSED tweet')
                else:
                    print('Sentiment:' + 'NON-DEPRESSED tweet')
```

### Output of testdata
Refer `output_testdata_prediction.csv` that will capture `2%` data used to test, system will use this to predict sentiment in `prediction` column.


## Further Improvements
- Use BM25 that will further improve accurancy and overall metrics.
- Use Twint and scape more data related to depression and include it in training set.
- This tool can be extended to use Map Reduce on Hadoop ecosystem and Amazon EKS to production scale.
- Add Web user interface using Flask or collect text from mobile devices, feed into this algorithm that can predict sentiment of the user.
- Use `TWINT` to scrape additional tweets to improve data set `twint -s "#depressed" --since 2018-05-15 -o depressive_tweets_processed.csv --csv`